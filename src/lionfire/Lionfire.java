package lionfire;

import lionfire.servlets.*;
import lionfire.sql.SQLManager;;

public class Lionfire {
	
	public static String AUTH = "123ABC";
	
	public static void main(String[] args)
	{
		SQLManager.Initialize();
		BridgeServlet.InitializeServer(new BridgeServlet[]{
				new ServerInfo(),
				new Login(),
				new CharLister(),
		});
	}
	
}
