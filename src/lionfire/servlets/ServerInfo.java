package lionfire.servlets;

import java.util.Map;

import lionfire.BridgeServlet;
import lionfire.util.Json;
import lionfire.util.JsonProperty;

public class ServerInfo extends BridgeServlet {

	public ServerInfo() {
		super("info");
	}

	@Override
	public Json Request(Map<String, String> query) {
		return new Json().setProperties(new JsonProperty[]{
				new JsonProperty("name", "Turtle"),
				new JsonProperty("region", "US-EAST"),
				new JsonProperty("population", "low"),
				new JsonProperty("speed", "normal"),
		}).setChildren(new Json[]{
				new Json("status").setProperties(new JsonProperty[]{
						new JsonProperty("running", "true"),
						new JsonProperty("problems", "false")
				}),
		});
	}

}
