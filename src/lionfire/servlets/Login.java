package lionfire.servlets;

import java.util.Map;

import lionfire.BridgeServlet;
import lionfire.active.SessionManager;
import lionfire.db.AccountAuth;
import lionfire.util.Json;
import lionfire.util.JsonProperty;

public class Login extends BridgeServlet {

	public Login() {
		super("login");
	}

	@Override
	public Json Request(Map<String, String> query) {
		Json json = new Json();
		
		//example json
		// { "type": "login", "username": "myUsername", "password": "myPassword" }
		System.out.println(query.get("username"));
		System.out.println(query.get("password"));
		if(query.get("type").equals("login"))
		{
			String username = query.get("username");
			String password = query.get("password");
			
			AccountAuth.AuthResponse response = AccountAuth.Login(username, password);
			json.properties = new JsonProperty[]{
					new JsonProperty("type", response.type.toString()),
					new JsonProperty("session", response.sessionID + ""),
			};

			SessionManager.AddSession(response.sessionID, username);
		} else if(query.get("type").equals("register"))
		{
			String email = query.get("email");
			String username = query.get("username");
			String password = query.get("password");
			
			AccountAuth.AuthResponse response = AccountAuth.Register(email, username, password);
			json.properties = new JsonProperty[]{
					new JsonProperty("type", response.type.toString()),
					new JsonProperty("session", response.sessionID + ""),
			};
			SessionManager.AddSession(response.sessionID, username);
		}
		
		return json;
	}

}
