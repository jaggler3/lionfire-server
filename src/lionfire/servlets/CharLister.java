package lionfire.servlets;

import java.util.Map;

import lionfire.BridgeServlet;
import lionfire.active.GameSession;
import lionfire.active.SessionManager;
import lionfire.db.AccountUtils;
import lionfire.db.CharacterInfo;
import lionfire.util.Json;
import lionfire.util.JsonProperty;

public class CharLister extends BridgeServlet {

	public CharLister() {
		super("charlist");
	}

	@Override
	public Json Request(Map<String, String> query) {
		Json response = new Json();
		
		int sessionID = Integer.parseInt(query.get("sessionID"));
		GameSession game;
		if((game = SessionManager.IsActive(sessionID)) != null)
		{
			int[] chars = AccountUtils.GetCharacters(game.accountName);
			CharacterInfo[] charData = new CharacterInfo[chars.length];
			for(int i = 0; i < chars.length; i++)
			{
				charData[i] = AccountUtils.GetCharacter(chars[i]);
			}
			
			response.setProperties(new JsonProperty[]{
					new JsonProperty("charCount", chars.length + ""),
			});
			response.setChildren(new Json[]{
					new Json("characters")
			});

			response.children[0].setChildren(new Json[chars.length]);
			for(int i = 0; i < chars.length; i++)
			{
				response.children[0].children[i] = new Json("" + i);
				response.children[0].children[i].setProperties(new JsonProperty[]{
						new JsonProperty("name", charData[i].name),
						new JsonProperty("location", charData[i].location),
						new JsonProperty("experience", charData[i].experience + ""),
						new JsonProperty("class", charData[i].getClassName()),
				});
			}
		}
		
		//TODO: add logic for when the session id is not valid
		
		return response;
	}
	
}
