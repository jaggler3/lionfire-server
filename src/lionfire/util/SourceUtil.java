package lionfire.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SourceUtil {
	
	public static String getSource(File file) throws Exception
	{
		String src = "";
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = "";
		if((src = reader.readLine()) == null)
		{
			src = "";
		}
		while((line = reader.readLine()) != null)
		{
			src += "\n" + line;
		}
		
		return src;
	}
	public static String getSource(InputStream stream) throws Exception
	{
		BufferedInputStream bis = new BufferedInputStream(stream);
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		int result = bis.read();
		while (result != -1)
		{
			byte b = (byte) result;
			buf.write(b);
			result = bis.read();
		}        
	    return buf.toString();
	}

}
