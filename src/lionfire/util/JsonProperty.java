package lionfire.util;

public class JsonProperty {
	public String name;
	public String data;
	public JsonProperty() { }
	public JsonProperty(String name, String data)
	{
		this.name = name;
		this.data = data;
	}
}
