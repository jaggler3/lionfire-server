package lionfire.util;

import java.io.File;
import java.util.HashMap;

import lionfire.script.Script;

public class Json {
	
	public String name;
	public JsonProperty[] properties = new JsonProperty[0];
	public Json[] children = new Json[0];
	
	private static Script parser;
	private static Script creator;
	
	static {
		parser = new Script();
		creator = new Script();
		try {
			parser.updateSource(SourceUtil.getSource(new File("res/jsonparser.js")));
			creator.updateSource(SourceUtil.getSource(new File("res/jsoncreator.js")));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Json() {}
	public Json(String name)
	{
		this.name = name;
	}
	public void initProperties(int size)
	{
		this.properties = new JsonProperty[size];
	}
	public void setPropertyAt(int index, JsonProperty property)
	{
		this.properties[index] = property;
	}
	
	@Override
	public String toString()
	{
		return Json.Serialize(this);
	}
	
	public static Json Read(String data)
	{
		Json json = new Json();
		Holder holder = new Holder(json);
		creator.context = new HashMap<String, Object>();
		creator.context.put("ROOT", holder);
		creator.context.put("DATA", data);
		creator.context.put("Creator", new Creator());
		
		creator.Execute(true);
		
		return (Json)holder.data;
	}
	
	public static class Holder {
		public Object data = new Object();
		public Holder(Object data) { this.data = data; };
	}

	public static class Creator {
		public JsonProperty[] CreatePropertyList(int size)
		{
			return new JsonProperty[size];
		}
		public Json[] CreateJsonList(int size)
		{
			return new Json[size];
		}
		public Json CreateJson()
		{
			return new Json();
		}
		public JsonProperty CreateProperty()
		{
			return new JsonProperty();
		}
		public JsonProperty CreateProperty(String name, String data)
		{
			return new JsonProperty(name, data);
		}
	}
	
	private static String Serialize(Json json)
	{
		String output = "";
		parser.context = new HashMap<String, Object>();
		Holder holder = new Holder(output);
		parser.context.put("ROOT", json);
		parser.context.put("DATA", holder);
		
		parser.Execute(true);
		
		return (String)holder.data;
	}

	public String getName() {
		return name;
	}

	public Json setName(String name) {
		this.name = name;
		return this;
	}

	public Json setProperties(JsonProperty[] properties) {
		this.properties = properties;
		return this;
	}

	public Json setChildren(Json[] children) {
		this.children = children;
		return this;
	}
}