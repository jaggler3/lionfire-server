package lionfire.script;

import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class Script {
	
	private static ScriptEngineManager engineManager;
	private ScriptEngine nashorn;
	
	private String source = "";
	public String getSource() { return source; };
	public HashMap<String, Object> context = new HashMap<String, Object>();
	
	public void updateSource(String source)
	{
		this.source = source;

		nashorn = engineManager.getEngineByName("JavaScript");
	}
	
	static {
		engineManager = new ScriptEngineManager();
	}
	
	public void Execute(boolean clean)
	{
		if(nashorn == null)
		{
			nashorn = engineManager.getEngineByName("JavaScript");
		}
		
		//add context variables
		for(Map.Entry<String, Object> entry : context.entrySet())
		{
			nashorn.put(entry.getKey(), entry.getValue());
		}

		try {
			nashorn.eval(source);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(clean)
		{
			for(Map.Entry<String, Object> entry : context.entrySet())
			{
				nashorn.put(entry.getKey(), "undefined");
			}
			context.clear();
		}
	}

}
