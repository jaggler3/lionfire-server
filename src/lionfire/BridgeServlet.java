package lionfire;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import lionfire.util.Json;

public abstract class BridgeServlet implements HttpHandler {
	
	public static HttpServer server;
	public String context;
	public abstract Json Request(Map<String, String> query);
	
	public static void InitializeServer(BridgeServlet[] servlets)
	{
		try
		{
			server = HttpServer.create(new InetSocketAddress(4000), 0);
	        server.setExecutor(null);
	        for(int i = 0; i < servlets.length; i++)
	        {
	            server.createContext("/" + servlets[i].context, servlets[i]);
	        }
	        server.start();
		} catch (Exception e) { e.printStackTrace(); }
	}
	
	public BridgeServlet(String context)
	{
		this.context = context;
	}
	
	@Override
	public void handle(HttpExchange exchange) throws IOException {
		String response = "";
		String query = exchange.getRequestURI().getRawQuery();
		
		if(query == null)
		{
			System.err.println("WARNING: NO AUTH KEY PROVIDED -- POSSIBLE INTRUSION");
			return;
		} else
		{
			Map<String, String> qd = ParseQuery(query);
			if(qd.containsKey("auth"))
			{
				String authKey = qd.get("auth");
				if(authKey.equals(Lionfire.AUTH))
				{
					qd.remove("auth");
					response = Request(qd).toString();
				} else
				{
					System.err.println("WARNING: INVALID AUTH KEY -- POSSIBLE INTRUSION (KEY: " + authKey + ")");
					return;
				}
			} else
			{
				System.err.println("WARNING: NO AUTH KEY PROVIDED -- POSSIBLE INTRUSION");
				return;
			}
		}
		
		exchange.sendResponseHeaders(200, response.length());
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
	}
	
	public Map<String, String> ParseQuery(String query) {
		Map<String, String> result = new HashMap<String, String>();
		for (String param : query.split("&")) {
			String pair[] = param.split("=");
			if (pair.length > 1) {
				result.put(pair[0], pair[1]);
			} else {
				result.put(pair[0], "");
			}
		}
		return result;
	}

}
