package lionfire.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLManager {
	
	private static Connection connection;
	
	public static void Initialize()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			connection = DriverManager.getConnection("jdbc:mysql://localhost/lionfire-main","root", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void CloseConnection()
	{
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static Object[][] Query(String query)
	{
	    try {
			ResultSet rs = CreateStatement().executeQuery(query);
			Object[][] result;
			int rowCount = 0;
			while(rs.next())
			{
				rowCount++;
			}
			while(rs.previous()) { } //rewind
			
			int columnCount = rs.getMetaData().getColumnCount();
			
			result = new Object[rowCount][columnCount];
			for(int i = 0; i < rowCount; i++)
			{
				rs.next();
				for(int j = 0; j < columnCount; j++)
				{
					result[i][j] = rs.getObject(j + 1);
				}
			}
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	    return null;
	}
	
	public static boolean Update(String sql)
	{
		try {
			connection.createStatement().execute(sql);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static Statement CreateStatement()
	{
		try {
			return connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
