package lionfire.db;

import lionfire.sql.*;

public class AccountUtils {
	
	public static int[] GetCharacters(String username)
	{
		int[] res;
		
		Object[][] qr = SQLManager.Query("SELECT `charids` FROM `accounts` WHERE `uname`='" + username + "'");
		if(qr.length == 0)
		{
			return new int[]{};
		}
		String chars = (String)qr[0][0];
		
		String[] spl = chars.split(",");
		
		res = new int[spl.length];
		
		for(int i = 0; i < spl.length; i++)
		{
			res[i] = Integer.parseInt(spl[i]);
		}
		
		return res;
	}
	
	public static CharacterInfo GetCharacter(int charID)
	{
		CharacterInfo info = new CharacterInfo();
		
		Object[][] qr = SQLManager.Query("SELECT `name`,`location`,`experience`,`class` FROM `characters` WHERE `charid`='" + charID + "'");
		
		String name = (String)qr[0][0];
		String location = (String)qr[0][1];
		int experience = (int)qr[0][2];
		String _class = (String)qr[0][3];
		
		info.name = name;
		info.location = location;
		info.experience = experience;
		info._class = _class;
		
		return info;
	}

}
