package lionfire.db;

import lionfire.sql.SQLManager;
import lionfire.util.JsonProperty;

public class AccountAuth {
	
	public enum AuthResponseType {
		Wrong, //wrong password or username
		Active, //account is already signed in //TODO
		Approve, //you are signed in
	}
	
	public static class AuthResponse {
		public AuthResponseType type;
		public int sessionID;
		public AuthResponse(AuthResponseType type, int sessionID)
		{
			this.type = type;
			this.sessionID = sessionID;
		}
	}
	
	public static AuthResponse Login(String username, String password)
	{
		if(username == null || username.isEmpty() || username.trim().isEmpty() || 
				password == null || password.isEmpty() || password.trim().isEmpty())
		{
			return new AuthResponse(AuthResponseType.Wrong, 0);
		}
		
		Object[][] sqlRequest = SQLManager.Query("SELECT `DBID` FROM `accounts` WHERE `uname`='" + username + "' AND `pword`='" + password + "'");
		boolean login = sqlRequest.length > 0;
		if(login)
		{
			return new AuthResponse(AuthResponseType.Approve, GenerateSessionID());
		} else
		{
			return new AuthResponse(AuthResponseType.Wrong, 0);
		}
	}
	
	public static AuthResponse Register(String email, String username, String password)
	{
		if(username == null || username.isEmpty() || username.trim().isEmpty() || 
				password == null || password.isEmpty() || password.trim().isEmpty() ||
						email == null || email.isEmpty() || email.trim().isEmpty())
		{
			return new AuthResponse(AuthResponseType.Wrong, 0);
		}
		
		System.out.println("ayyy");
		String exe = "INSERT INTO `accounts`"
				+ " (`email`, `uname`, `pword`, `charids`, `banned`) "
				+ "VALUES ('" + email + "', '" + username + "', '" + password + "', '', '0')";
		boolean sqlUpdate = SQLManager.Update(exe);
		System.out.println("lmao");
		if(sqlUpdate)
		{
			return new AuthResponse(AuthResponseType.Approve, GenerateSessionID());
		} else
		{
			return new AuthResponse(AuthResponseType.Wrong, 0);
		}
	}
	
	public static int GenerateSessionID()
	{
		return (int)(Math.random() * 899999999) + 10000000;
	}

}
