package lionfire.db;

public class CharacterInfo {
	
	public String name;
	public String location;
	public int experience;
	public int charID;
	public String _class;
	
	public CharacterInfo() { }
	
	/*
	 * returns the class of this character
	 */
	public String getClassName()
	{
		return _class;
	}
	
}
