package lionfire.active;

import java.util.ArrayList;
import java.util.List;

public class SessionManager {
	
	public static List<GameSession> sessions = new ArrayList<GameSession>();
	
	public static void AddSession(int session, String username)
	{
		sessions.add(new GameSession(session, username));
	}
	
	public static GameSession IsActive(int sessionID)
	{
		for(int i = 0; i < sessions.size(); i++)
		{
			if(sessions.get(i).sessionID == sessionID)
			{
				return sessions.get(i);
			}
		}
		
		return null;
	}

}
