package lionfire.active;

public class GameSession {
	
	public int sessionID;
	public Player player;
	public String accountName;
	private boolean playing = false;
	
	public GameSession(int sessionID, String accountName)
	{
		this.sessionID = sessionID;
		this.accountName = accountName;
	}
	
	public void setPlaying(Player player)
	{
		this.player = player;
		this.playing = true;
	}

	public boolean isPlaying()
	{
		return playing;
	}
}
