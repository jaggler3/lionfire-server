package lionfire.active;

import lionfire.db.CharacterInfo;

public class Player {
	
	public int sessionID;
	public CharacterInfo character;
	
	public Player(int sessionID, CharacterInfo character)
	{
		this.sessionID = sessionID;
		this.character = character;
	}

}
