> usernames can only be 255 characters or less
> passwords can only be 255 characters or less

'Internal Server Error' (when the server says "INVALID") diagnostics:
	* Check the internal request logs to see if the URI's contain any unwanted characters
	* Check if the Servlet is responding with the right text