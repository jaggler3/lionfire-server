var object = construct(ROOT);
DATA.data = JSON.stringify(object);

function construct(a)
{
	var object = {};
	
	if(a.properties && a.properties.length > 0)
	{
		for(var i = 0; i < a.properties.length; i++)
		{
			object[a.properties[i].name] = a.properties[i].data;
		}
	}
	
	
	if(a.children != null && a.children.length > 0)
	{
		for(var i = 0; i < a.children.length; i++)
		{
			object[a.children[i].name] = construct(a.children[i]);
		}
	}
	
	
	return object;
}