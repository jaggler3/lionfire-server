var object = JSON.parse(DATA);
ROOT.data = deconstruct(object, true);

function deconstruct(object, r)
{
	var a = Creator.CreateJson();
	
	var propertyKeys = [];
	var propertyObjects = [];
	var childrenKeys = [];
	var childrenObjects = [];
	
	if(!r)
	{
		a.name = object.name;
	}
	
	for(var key in object)
	{
		if(object[key].constructor === Object)
		{
			childrenKeys.push(key);
			childrenObjects.push(object[key]);
		} else
		{
			propertyKeys.push(key);
			propertyObjects.push(object[key]);
		}
	}
	
	if(propertyKeys.length > 0)
	{
		a.initProperties(propertyKeys.length);
		for(var i in propertyKeys)
		{
			a.setPropertyAt(i, Creator.CreateProperty(propertyKeys[i], propertyObjects[i]));
		}
	}
	
	if(childrenKeys.length > 0)
	{
		a.children = Creator.CreateJsonList(childrenKeys.length);
		for(var i in childrenKeys)
		{
			var obj = deconstruct(childrenObjects[i], false);
			a.children[i] = obj;
			a.children[i].name = childrenKeys[i];
		}
	}
	
	return a;
}